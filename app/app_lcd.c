#include "app_bsp.h"
#include "app_lcd.h"


void MOD_LCD_Init( LCD_HandleTypeDef *hlcd ) 
{   
    MOD_LCD_MspInit( hlcd );                    

    HAL_GPIO_WritePin( hlcd->CsPort, hlcd->CsPin, SET ); //   CS = 1;
    HAL_GPIO_WritePin( hlcd->RstPort, hlcd->RstPin, RESET); // RST = 0;
    HAL_Delay( 2 );
    HAL_GPIO_WritePin( hlcd->RstPort, hlcd->RstPin, SET); // RST = 1;
    HAL_Delay( 20 );
    MOD_LCD_Command( hlcd, 0x30 ); // wakeup 
    HAL_Delay( 2 );
    MOD_LCD_Command( hlcd, 0x30 );
    MOD_LCD_Command( hlcd, 0x30 );
    MOD_LCD_Command( hlcd, 0x39 ); // function set 
    MOD_LCD_Command( hlcd, 0x56); /*power control*/
    MOD_LCD_Command( hlcd, 0x6D); /*follower control*/
    HAL_Delay( 200 );
    MOD_LCD_Command( hlcd, 0x70 ); // constrast
    MOD_LCD_Command( hlcd, 0x0C ); // display on
    MOD_LCD_Command( hlcd, 0x06 ); // entry mode
    MOD_LCD_Command( hlcd, 0x01 ); // clear screen
    HAL_Delay( 1 );
}

void MOD_LCD_Command( LCD_HandleTypeDef *hlcd, uint8_t cmd )
{
    HAL_GPIO_WritePin(hlcd->RsPort, hlcd->RsPin, RESET);
    HAL_GPIO_WritePin( hlcd->CsPort, hlcd->CsPin, RESET );
    HAL_SPI_Transmit(hlcd->SpiHandler, &cmd, 1, 1000);
    HAL_GPIO_WritePin( hlcd->CsPort, hlcd->CsPin, SET ); 
}

void MOD_LCD_Data( LCD_HandleTypeDef *hlcd, uint8_t data )
{
    HAL_GPIO_WritePin(hlcd->RsPort, hlcd->RsPin, SET);
    HAL_GPIO_WritePin( hlcd->CsPort, hlcd->CsPin, RESET );
    HAL_SPI_Transmit(hlcd->SpiHandler, &data, 1, 1000);
    HAL_GPIO_WritePin( hlcd->CsPort, hlcd->CsPin, SET );
}

void MOD_LCD_SetCursor( LCD_HandleTypeDef *hlcd, uint8_t row, uint8_t col )
{
    uint8_t cursor;
    if (row == 0)
    {
        cursor = 0x80 + col;
    }
    if(row == 1)
    {
        cursor = 0xC0 + col;
    }
    MOD_LCD_Command(hlcd, cursor);
}
 
void MOD_LCD_String( LCD_HandleTypeDef *hlcd, char *str )
{
    HAL_GPIO_WritePin(hlcd->RsPort, hlcd->RsPin, SET);
    HAL_GPIO_WritePin( hlcd->CsPort, hlcd->CsPin, RESET );
    HAL_SPI_Transmit(hlcd->SpiHandler, (uint8_t*)str, strlen(str), 1000);
    HAL_GPIO_WritePin( hlcd->CsPort, hlcd->CsPin, SET );
}

__weak void MOD_LCD_MspInit( LCD_HandleTypeDef *hlcd )
{

}