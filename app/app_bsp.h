#ifndef _BSP_H_
#define _BSP_H_

#include "stm32f0xx.h"
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <app_lcd.h>

#define NONE  0x00
#define TIME  0x01
#define DATE  0x02
#define ALARM 0x03

#define HEART_BEAT_LED_PIN GPIO_PIN_5
#define HEART_BEAT_LED_PORT GPIOA

#define CS_PIN GPIO_PIN_1
#define RS_PIN GPIO_PIN_2
#define RST_PIN GPIO_PIN_11
#define LCDPORT GPIOB
#define BOTON_PIN GPIO_PIN_13

#define NOEMPTY 0x00
#define EMPTY   0x01
#define FULL    0x01
#define NOFULL  0x00

// poner prioridad de interrupciones
#define SIZERXBUFFER 0x74

typedef struct 
{
    uint8_t msg;     // tipo de mensaje
    uint8_t param1;  // hora o dia
    uint8_t param2;  // minutos o mes
    uint16_t param3; // segundos o año
} SERIAL_MsgTypeDef;

#endif
