#include "app_bsp.h"
#include "app_serial.h"
#include "app_queue.h"

#define IDLE 0x00
#define TOK 0x01
#define VALIDCOMMAND 0x02
#define COMMANDERROR 0x03
#define COMMANDTIME 0x04
#define COMMANDDATE 0x05
#define COMMANDALARM 0x06

UART_HandleTypeDef UartHandle;
SERIAL_MsgTypeDef Serial_transfer;
QUEUE_HandleTypeDef Serial_Queue;
//SERIAL_MsgTypeDef buffer[10];
//SERIAL_MsgTypeDef MsgToSend;

void token(void);
uint32_t str_to_int(char *str);
uint8_t comand_valid();

uint8_t SerialBuffer[100];
uint8_t SerialByte;

uint8_t tickSerial;
char cpyRxBuf[40];
char Rxbuff_Uart[20];
char *tok1;
char *tok2;
char *tok3;
char *tok4;

void serial_task(void)
{
    static uint8_t selection = IDLE;
    uint8_t data;
    uint8_t index = 0;
    
    switch (selection)
    {
    case IDLE:
        
        if ( (HAL_GetTick() - tickSerial) >= 10)
        {
            
            while (HIL_QUEUE_IsEmpty(&Serial_Queue) == NOEMPTY) 
            {
                HAL_UART_Transmit(&UartHandle, (uint8_t*)"X\n", 2, 1000);
                HAL_GPIO_WritePin(GPIOA, HEART_BEAT_LED_PIN, SET);
                HAL_NVIC_DisableIRQ(USART2_IRQn);
                HIL_QUEUE_Read(&Serial_Queue, &data); 
                HAL_NVIC_EnableIRQ(USART2_IRQn);

                if (data == '\r') 
                {
                   
                    selection = TOK;
                    break;
                }
                else
                {
                    Rxbuff_Uart[index++] = data;
                }       
            }
            tickSerial = HAL_GetTick();
        }
        
        break;
    case TOK:
        token();
        memset(Rxbuff_Uart, '\0', sizeof(Rxbuff_Uart));
        selection = VALIDCOMMAND;
        break;
    case VALIDCOMMAND:
        selection = comand_valid();
        break;
    case COMMANDTIME:
        HAL_UART_Transmit(&UartHandle, (uint8_t *)("\nOK\n"), 4, 250);
        Serial_transfer.msg = TIME;
        Serial_transfer.param1 = str_to_int(tok2);
        Serial_transfer.param2 = str_to_int(tok3);
        Serial_transfer.param3 = str_to_int(tok4);
        selection = IDLE;
        break;
    case COMMANDDATE:
        HAL_UART_Transmit(&UartHandle, (uint8_t *)("\nOK\n"), 4, 250);
        Serial_transfer.msg = DATE;
        Serial_transfer.param1 = str_to_int(tok2);
        Serial_transfer.param2 = str_to_int(tok3);
        Serial_transfer.param3 = str_to_int(tok4);
        selection = IDLE;
        break;
    case COMMANDALARM:
        HAL_UART_Transmit(&UartHandle, (uint8_t *)("\nOK\n"), 4, 250);
        Serial_transfer.msg = ALARM;
        Serial_transfer.param1 = str_to_int(tok2);
        Serial_transfer.param2 = str_to_int(tok3);
        Serial_transfer.param3 = str_to_int(tok4);
        selection = IDLE;
        break;
    case COMMANDERROR:
        HAL_UART_Transmit(&UartHandle, (uint8_t *)("\nERROR\n"), 7, 250);
        selection = IDLE;
        break;
    }
}

void serial_Init(void)
{
    tickSerial = HAL_GetTick();
    UartHandle.Init.BaudRate = 115200; // 1/115200 = 8.68u * 10ms = 86u    10ms/86us = 116 
    UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
    UartHandle.Init.StopBits = UART_STOPBITS_1;
    UartHandle.Init.Parity = UART_PARITY_NONE;
    UartHandle.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    UartHandle.Init.Mode = UART_MODE_TX_RX;
    UartHandle.Instance = USART2;
    UartHandle.Init.OverSampling = UART_OVERSAMPLING_16;
    HAL_UART_Init(&UartHandle);

    HAL_UART_Receive_IT(&UartHandle, &SerialByte, 1);

    Serial_Queue.Buffer = (void*)SerialBuffer;
    Serial_Queue.Elements = 100;
    Serial_Queue.Size = sizeof(uint8_t);
    HIL_QUEUE_Init(&Serial_Queue);
    
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    HIL_QUEUE_Write(&Serial_Queue, &SerialByte);
    HAL_UART_Receive_IT(&UartHandle, &SerialByte, 1);
}

uint32_t str_to_int(char *str)
{
    uint32_t num = 0;
    for (uint8_t i = 0; str[i] != '\0'; ++i) 
    {
        num = num * 10 + str[i] - '0'; 
    }
    return num; 
}

void token(void)
{
    strcpy(cpyRxBuf, Rxbuff_Uart);
    tok1 = strtok(cpyRxBuf, "=");
    tok2 = strtok(NULL, ",");
    tok3 = strtok(NULL, ",");
    tok4 = strtok(NULL, " ");
}

uint8_t comand_valid()
{
    if (strcmp(tok1, "AT+TIME") == 0 && str_to_int(tok2) <= 23 && str_to_int(tok3) <= 59 && str_to_int(tok4) <= 59)
    {
        return COMMANDTIME;
    }
    else if (strcmp(tok1, "AT+DATE") == 0 && str_to_int(tok2) <= 31 && str_to_int(tok2) > 0 && str_to_int(tok3) <= 12 && str_to_int(tok3) > 0 && str_to_int(tok4) >= 0 && str_to_int(tok4) < 99)
    {
        return COMMANDDATE;
    }
    else if (strcmp(tok1, "AT+ALARM") == 0 && str_to_int(tok2) <= 23 && str_to_int(tok3) <= 59 && str_to_int(tok4) <= 59)
    {
        return COMMANDALARM;
    }
    return COMMANDERROR;
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
    
}