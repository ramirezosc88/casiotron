#include "app_bsp.h"
#include "app_queue.h"


void HIL_QUEUE_Init( QUEUE_HandleTypeDef *hqueue )
{
    hqueue->Head = 0;
    hqueue->Tail = 0;
    hqueue->Empty = 1;
    hqueue->Full = 0;
}

uint8_t HIL_QUEUE_Write( QUEUE_HandleTypeDef *hqueue, void *data ) // { 1 , 2 ,  ,}
{     
    uint8_t value;
    if (hqueue->Full == NOFULL) // void *memcpy(void *dest, const void * src, size_t n)   int arr[10];
    {
        hqueue->Empty = NOEMPTY;//                                                        h
        memcpy( hqueue->Buffer + hqueue->Head, data, hqueue->Size ); // {  2 ,  3 ,  4  ,   ,    }    { 5 , 7 }       &arr[1];
        hqueue->Head += hqueue->Size % hqueue->Elements;  
        value = 1;
    }
    else
    {
        value = 0;
    }
    if (hqueue->Head == hqueue->Tail)
    {
        hqueue->Full = FULL;
    }
    return value;
}

uint8_t HIL_QUEUE_Read( QUEUE_HandleTypeDef *hqueue, void *data )
{
    uint8_t value;
    if (hqueue->Empty == NOEMPTY)
    {
        hqueue->Full = NOFULL;
        memcpy( &data, hqueue->Buffer + hqueue->Tail, hqueue->Size);
        hqueue->Tail += hqueue->Size % hqueue->Elements;
        value = 1;
    }
    else
    {
        value = 0;
    }    
    if (hqueue->Tail == hqueue->Head)
    {
        hqueue->Empty = EMPTY;
    }
    return value;
}

uint8_t HIL_QUEUE_IsEmpty( QUEUE_HandleTypeDef *hqueue )
{
    return hqueue->Empty;
}