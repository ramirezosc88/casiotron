#include "app_bsp.h"

extern uint8_t alarmInitiated;
extern UART_HandleTypeDef UartHandle;
extern RTC_HandleTypeDef RtcHandle;
extern WWDG_HandleTypeDef WwdgHandle;

void NMI_Handler(void)
{
}


void HardFault_Handler(void)
{
    assert_param(0u);
}


void SVC_Handler(void)
{
}


void PendSV_Handler(void)
{
}


void SysTick_Handler(void)
{
    HAL_IncTick();
}

void USART2_IRQHandler(void)
{
    HAL_UART_IRQHandler(&UartHandle);
}

void RTC_IRQHandler(void)
{
    alarmInitiated = 0;
    HAL_RTC_AlarmIRQHandler(&RtcHandle);
    HAL_RTC_DeactivateAlarm(&RtcHandle, RTC_ALARM_A);
}

void EXTI4_15_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_13);
}

// void WWDG_IRQHandler(void)
// {
//     HAL_WWDG_IRQHandler(&WwdgHandle);
// }
