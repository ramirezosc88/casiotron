#include "app_bsp.h"
#include "app_clock.h"
#include "app_lcd.h"

#define IDLE 0x00
#define STATETIME 0x01
#define STATEDATE 0x02
#define STATEALARM 0x03
#define STATENONE 0x04
#define STATESHOWTIME 0x05
#define STATEALARMACT 0x06
#define STATESHOWALAR 0x07

SPI_HandleTypeDef SpiHandle;
LCD_HandleTypeDef LCDHandle;
extern SERIAL_MsgTypeDef Serial_transfer;
RTC_HandleTypeDef RtcHandle;

const char *strweek[] = {"", "MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"};
const char *strmonth[] = {"", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP","OCT", "NOV", "DEC"};

uint8_t alarmInitiated = 0;
void int_to_str(uint8_t num, char *str);
uint8_t zeller(uint8_t day, uint8_t month, uint16_t year);
void show_Alarm(void);
uint8_t selectmsg(void);
void set_Time(void);
void set_Alarm(void);
void set_Date(void);
void show_Time(void);

uint32_t tickShowTime;
uint32_t tickAlarmActiv;
uint32_t tickShowAlarm;
bool alarmActivated = false;
bool buttonOn = false;

void clock_task(void)
{
    static uint8_t seg = 0;
    static uint8_t state = IDLE;

    switch (state)
    {
    case IDLE:
        if (buttonOn)
        {
            state = STATESHOWALAR;
        }
        if ((HAL_GetTick() - tickShowTime) >= 1000)
        {
            state = STATESHOWTIME;
        }
        if (alarmActivated)
        {
            state = STATEALARMACT;
        }
        if (Serial_transfer.msg != NONE)
        {
            state = selectmsg();
        }
        break;
    case STATETIME:
        set_Time();
        Serial_transfer.msg = NONE;
        state = IDLE;
        break;
    case STATEDATE:
        set_Date();
        Serial_transfer.msg = NONE;
        state = IDLE;
        break;
    case STATEALARM:
        set_Alarm();
        Serial_transfer.msg = NONE;
        state = IDLE;
        break;
    case STATENONE:
        state = IDLE;
        break;
    case STATESHOWTIME:
        show_Time();
        tickShowTime = HAL_GetTick();
        state = IDLE;
        break;
    case STATEALARMACT:
        if (seg < 60 && alarmActivated == 1 && (HAL_GetTick() - tickAlarmActiv) >= 1000)
        {
            show_Time();
            tickAlarmActiv = HAL_GetTick();
            seg++;
        }
        if (seg > 59 || alarmActivated == 0)
        {
            seg = 0;
            alarmActivated = false;
            state = IDLE;
        }
        break;
    case STATESHOWALAR:
        if ((HAL_GetTick() - tickShowAlarm) >= 1000)
        {
            show_Alarm();
            tickShowAlarm = HAL_GetTick();
        }
        if (HAL_GPIO_ReadPin(GPIOC, BOTON_PIN) == 0)
        {
            state = STATESHOWALAR;
        }
        else
        {
            buttonOn = false;
            state = IDLE;
        }
        break;
    default:
        break;
    }
}

void show_Time(void)
{
    RTC_DateTypeDef datestrct;
    RTC_TimeTypeDef timestrct;
    static uint8_t toggle = 0;
    char date[20] = " ";
    char time[20];
    char temp[20];
    HAL_RTC_GetTime(&RtcHandle, &timestrct, RTC_FORMAT_BIN);
    HAL_RTC_GetDate(&RtcHandle, &datestrct, RTC_FORMAT_BIN);

    strcpy(time, "    ");
    int_to_str(timestrct.Hours, temp);
    strcat(time, temp);
    strcat(time, ":");
    int_to_str(timestrct.Minutes, temp);
    strcat(temp, ":");
    strcat(time, temp);
    int_to_str(timestrct.Seconds, temp);
    strcat(time, temp);
    
    if (alarmInitiated == 1)
    {
        strcat(time, "  A ");
    }
    else if (alarmActivated == 1 && toggle == 0)
    {
        toggle = 1;
        strcpy(time, "*** ");
        int_to_str(timestrct.Hours, temp);
        strcat(time, temp);
        strcat(time, ":");
        int_to_str(timestrct.Minutes, temp);
        strcat(temp, ":");
        strcat(time, temp);
        int_to_str(timestrct.Seconds, temp);
        strcat(time, temp);
        strcat(time, " ***");
    }
    else
    {
        strcat(time, "    ");
        toggle = 0;
    }

    strcat(date, strmonth[datestrct.Month]);
    strcat(date, ",");
    int_to_str(datestrct.Date, temp);
    strcat(date, temp);
    strcat(date, ",20");
    int_to_str(datestrct.Year, temp);
    strcat(date, temp);
    strcat(date, " ");
    strcat(date, strweek[datestrct.WeekDay]);

    MOD_LCD_SetCursor(&LCDHandle, 0x00, 0x00);
    MOD_LCD_String(&LCDHandle, date);
    MOD_LCD_SetCursor(&LCDHandle, 0x01, 0x00);
    MOD_LCD_String(&LCDHandle, time);
}

void show_Alarm(void)
{
    char temp[10];
    char alarm[20] = "ALARM ";
    RTC_AlarmTypeDef alarmstruct;
    HAL_RTC_GetAlarm(&RtcHandle, &alarmstruct, RTC_ALARM_A, RTC_FORMAT_BIN);

    if (alarmInitiated == 1)
    {
        int_to_str(alarmstruct.AlarmTime.Hours, temp);
        strcat(alarm, temp);
        strcat(alarm, ":");
        int_to_str(alarmstruct.AlarmTime.Minutes, temp);
        strcat(alarm, temp);
        strcat(alarm, ":00  ");
    }
    else
    {
        strcpy(alarm, " NO ALARM CONFIG");
    }

    MOD_LCD_SetCursor(&LCDHandle, 0x01, 0x00);
    MOD_LCD_String(&LCDHandle, alarm);
}

uint8_t selectmsg(void)
{
    switch (Serial_transfer.msg)
    {
    case TIME:
        return STATETIME;
        break;
    case DATE:
        return STATEDATE;
        break;
    case ALARM:
        return STATEALARM;
        break;
    }
    return STATENONE;
}

void int_to_str(uint8_t num, char *str)
{
    char arr[2] = "0";
    uint8_t numtemp = num;
    uint8_t base = 10;
    uint8_t i = 0;
    char rev[100];
    uint8_t len;
    uint8_t count = 0;
    uint8_t j = 0;

    if (num == 0)
    {
        str[i++] = '0';
        str[i++] = '0';
        str[i] = '\0';
        return;
    }
    while (num != 0)
    {
        int rem = num % base;
        str[i++] = (rem > 9) ? (rem - 10) + 'a' : rem + '0';
        num = num / base;
    }
    str[i] = '\0';
    if (numtemp < 10)
    {
        strcat(str, arr);
    }
    while (str[count] != '\0')
    {
        count++;
    }
    len = count - 1;
    for (; j < count; j++)
    {
        rev[j] = str[len];
        len--;
    }
    strcpy(str, rev); 
    str[j] = '\0';
}

void set_Time(void)
{
    RTC_TimeTypeDef stimestructure;
    stimestructure.Hours = Serial_transfer.param1;
    stimestructure.Minutes = Serial_transfer.param2;
    stimestructure.Seconds = Serial_transfer.param3;
    stimestructure.TimeFormat = RTC_HOURFORMAT12_AM;
    stimestructure.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
    stimestructure.StoreOperation = RTC_STOREOPERATION_RESET;

    HAL_RTC_SetTime(&RtcHandle, &stimestructure, RTC_FORMAT_BIN);
}

void set_Alarm(void)
{
    RTC_AlarmTypeDef salarmstructure;

    salarmstructure.Alarm = RTC_ALARM_A;
    salarmstructure.AlarmDateWeekDay = RTC_WEEKDAY_WEDNESDAY;
    salarmstructure.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
    salarmstructure.AlarmMask = RTC_ALARMMASK_DATEWEEKDAY;
    salarmstructure.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_NONE;
    salarmstructure.AlarmTime.TimeFormat = RTC_HOURFORMAT12_AM;
    salarmstructure.AlarmTime.Hours = Serial_transfer.param1;
    salarmstructure.AlarmTime.Minutes = Serial_transfer.param2;
    salarmstructure.AlarmTime.Seconds = Serial_transfer.param3;
    salarmstructure.AlarmTime.SubSeconds = 0;
    alarmInitiated = 1;
    HAL_RTC_SetAlarm_IT(&RtcHandle, &salarmstructure, RTC_FORMAT_BIN);
}

void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
{
    alarmActivated = true;
}

void set_Date(void)
{
    uint8_t week;
    RTC_DateTypeDef sdatestructure;
    week = zeller(Serial_transfer.param1, Serial_transfer.param2, Serial_transfer.param3);
    if (week == 0)
    {
        week = 7;
    }
    sdatestructure.Date = Serial_transfer.param1;
    sdatestructure.Month = Serial_transfer.param2;
    sdatestructure.Year = Serial_transfer.param3;
    sdatestructure.WeekDay = week;
    HAL_RTC_SetDate(&RtcHandle, &sdatestructure, RTC_FORMAT_BIN);
}

void clock_Init(void)
{
    RtcHandle.Instance = RTC;
    RtcHandle.Init.HourFormat = RTC_HOURFORMAT_24;
    RtcHandle.Init.OutPut = RTC_OUTPUT_DISABLE;
    RtcHandle.Init.AsynchPrediv = 0x7F;
    RtcHandle.Init.SynchPrediv = 0x00FF;
    RtcHandle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
    RtcHandle.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
    HAL_RTC_Init(&RtcHandle);

    SpiHandle.Instance = SPI2;
    SpiHandle.Init.Mode = SPI_MODE_MASTER;
    SpiHandle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256; // 48MHz / 256 = 187500 -> 1 / 187500 = 5.33u * 8 =  42.6u
    SpiHandle.Init.Direction = SPI_DIRECTION_2LINES;
    SpiHandle.Init.CLKPhase = SPI_PHASE_2EDGE; //
    SpiHandle.Init.CLKPolarity = SPI_POLARITY_LOW;
    SpiHandle.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    SpiHandle.Init.CRCPolynomial = 0;
    SpiHandle.Init.DataSize = SPI_DATASIZE_8BIT;
    SpiHandle.Init.FirstBit = SPI_FIRSTBIT_MSB;
    SpiHandle.Init.NSS = SPI_NSS_SOFT;
    SpiHandle.Init.TIMode = SPI_TIMODE_DISABLE;
    HAL_SPI_Init(&SpiHandle);

    RTC_DateTypeDef sdatestructure;
    sdatestructure.Date = 1;
    sdatestructure.Month = 1;
    sdatestructure.Year = 0;
    sdatestructure.WeekDay = RTC_WEEKDAY_MONDAY;
    HAL_RTC_SetDate(&RtcHandle, &sdatestructure, RTC_FORMAT_BIN);

    LCDHandle.SpiHandler = &SpiHandle;
    LCDHandle.CsPort = LCDPORT;
    LCDHandle.RsPort = LCDPORT;
    LCDHandle.RstPort = LCDPORT;
    LCDHandle.CsPin = CS_PIN;
    LCDHandle.RsPin = RS_PIN;
    LCDHandle.RstPin = RST_PIN;
    MOD_LCD_Init(&LCDHandle);

    GPIO_InitTypeDef GPIO_InitStruct;
    __HAL_RCC_GPIOC_CLK_ENABLE();
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
    GPIO_InitStruct.Pin = BOTON_PIN; 
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    HAL_NVIC_SetPriority(EXTI4_15_IRQn ,1,0);
    HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    alarmActivated = false;
    buttonOn = true;
}

uint8_t zeller(uint8_t day, uint8_t month, uint16_t year)
{
    uint8_t h;
    uint8_t k;
    uint8_t j;

    if (month <= 2)
    {
        month += 12;
        year -= 1;
    }
    else
    {
        month -= 2;
    }
    k = year % 100;
    j = year / 100;
    h = ((700 + ((26 * month - 2) / 10) + day + k + (k / 4) + ((j / 4) + 5 * j)) % 7);
    return h;
}
