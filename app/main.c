#include "app_bsp.h"
#include "app_clock.h"
#include "app_serial.h"

#define REFTIMEWWDG 0x1E
#define TIMEHEARTLED 0x12C

WWDG_HandleTypeDef WwdgHandle;

static uint32_t tickheart = 0;
static uint32_t tickwwdg = 0;
void SystemClock_Config(void);
void heart_Init(void);
void heart_beat(void);
void dog_Init(void);
void peth_the_dog(void);

int main(void)
{
    HAL_Init();
    SystemClock_Config();
    heart_Init();
    serial_Init();
    clock_Init();
    dog_Init();
    
    while (1)
    {
        serial_task();
        clock_task();
        //heart_beat();
        peth_the_dog();
    }
}

void SystemClock_Config(void)
{
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_OscInitTypeDef RCC_OscInitStruct;

    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
    RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
    RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        while (1);
    }

    RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1);
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
    {
        while (1);
    }
}

void peth_the_dog(void)
{
    if ((HAL_GetTick() - tickwwdg) >= REFTIMEWWDG)
    {
        HAL_WWDG_Refresh(&WwdgHandle); 
        tickwwdg = HAL_GetTick();
    }
}

void dog_Init(void)
{   //1464.84 = 6826 us total 29ms
    WwdgHandle.Instance = WWDG;
    WwdgHandle.Init.Prescaler = WWDG_PRESCALER_8;
    WwdgHandle.Init.Window = 85;  // 
    WwdgHandle.Init.Counter = 127; //
    WwdgHandle.Init.EWIMode = WWDG_EWI_DISABLE;
    HAL_WWDG_Init(&WwdgHandle);
    tickwwdg = HAL_GetTick();
}

void heart_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    __HAL_RCC_GPIOA_CLK_ENABLE();

    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Pin = HEART_BEAT_LED_PIN;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    HAL_GPIO_WritePin(GPIOA, HEART_BEAT_LED_PIN, RESET);
    tickheart = HAL_GetTick();
}

void heart_beat(void)
{ 
    if ((HAL_GetTick() - tickheart) >= TIMEHEARTLED)
    {
        HAL_GPIO_TogglePin(HEART_BEAT_LED_PORT, HEART_BEAT_LED_PIN);
        tickheart = HAL_GetTick();
    }
}